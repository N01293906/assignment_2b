﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DatabaseDesign.aspx.cs" Inherits="portfolio.DatabaseDesign" %>
<asp:Content ID="testcontainer" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-heading"><a href="DatabaseDesign.aspx" class="head-title">Database Design</a></h1>
    <h2>Tips for Better Database Design </h2>
    <ul>
        <li>Plan Ahead</li>
        <li>Document Your Model</li>
        <li>Follow Conventions</li>
        <li>Think Carefully About Keys</li>
        <li>Use Integrity Checks Carefully</li>
        <li>Don’t Forget Indexes in Your Design</li>
        <li>Avoid Common Lookup Tables</li>
        <li>Define an Archiving Strategy</li>
        <li>Test Early, Test Often</li>
    </ul>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart1" runat="server">
    <h2>Code Snippet</h2>
    <p><strong> need to start phoning and emailing clients that have invoices which are due in the
    next month. Find all the invoice information for invoices which are due in the next month,
    as well as the corresponding client’s phone number, email, full name. Order by lastname
    first, then highest invoices to lowest. Only include rows with direct matches to clients and
    invoices.</strong></p>
    <uctrl:code runat="server" id="code1" Interest="db1" />
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart2" runat="server">
<h2>Create View Syntax</h2>
    <uctrl:code runat="server" id="code_display" Interest="db2" />
    
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart3" runat="server">
    <h2>Useful Links</h2>
    <div>
        <ul class="links">
            <li><a href="http://www.vertabelo.com/blog/notes-from-the-lab/9-tips-for-better-database-design">Tips</a></li>
            <li><a href="https://searchsecurity.techtarget.com/tip/Five-tips-for-secure-database-development">Security in Database Development</a></li>
        </ul>
    </div>
</asp:Content>
