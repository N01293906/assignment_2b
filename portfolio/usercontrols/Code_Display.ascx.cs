﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace portfolio.usercontrols
{
    public partial class Code_Display : System.Web.UI.UserControl
    {
        
        public string Interest
        {
            get { return (string)ViewState["Interest"]; }
            set { ViewState["Interest"] = value; }

        }
        DataView SourceCodeDisplay()
        {
            DataTable codeData = new DataTable();
            DataColumn idx_col = new DataColumn();
            DataColumn cod_col = new DataColumn();

            DataRow codeRow;
            idx_col.ColumnName = "LINE";
            cod_col.ColumnName = " CODE";

            idx_col.DataType = System.Type.GetType("System.Int32");
            cod_col.DataType = System.Type.GetType("System.String");

            codeData.Columns.Add(idx_col);
            codeData.Columns.Add(cod_col);/*THIS LINE IS ADDING CREATED COLUMN TO codeData */
            List<string> display_code;
            if (Interest == "webCode1")
            {
                 display_code = new List<string>(new string[]
                 {
                 "~<form id = \"form1\" runat = \"server\">",
                "~~<div>",
                "~~~<asp: Calendar ID = \"Calendar1\" runat = \"server\" OnSelectionChanged = \"Calendar1_SelectionChanged\">",
                "~~~</ asp:Calendar>",
                "~~~<asp: TextBox ID = \"TextBox1\" runat = \"server\">",
                "~~~</ asp:TextBox>",
                "~~</ div>",
                "~</ form>"
                 });
               
            }
            else if (Interest == "webCode2")
            {
                display_code = new List<string>(new string[]
                 {
                "~<div class=html>",
                "~~~<html><!--TO GET HTML CODE ON HTML PAGE REPLACE<with &lt; and > with &gt;-->",
                "~~~~<head>",
                "~~~~~<title>Title</title>",
                "~~~~</head>",
                "~~~<body>",
                "~~~~<p>Unrendred html</p>",
                "~~~</body>",
                "~~</html>",
                "~</div>"

                 });
               
            }
            else if (Interest == "db2")
            {
                display_code = new List<string>(new string[]
                 {
                     "~~CREATE VIEW view_name AS",
                     "~~SELECT column1, column2, ...",
                     "~~FROM table_name",
                     "~~WHERE condition;",
                 });
                
            }
            else if (Interest == "db1")
            {
                display_code = new List<string>(new string[]{
                 "~~select clients.clientid, (clientfname||' '|| clientlname) as \"Full name\", clientemail, clientphone,invoiceid, invoice_dueby, invoice_amount",
                 "~~from clients",
                 "~~inner join invoices on clients.clientid=invoices.clientid",
                 "~~where invoice_dueby> sysdate and invoice_dueby<'01-nov-2018'",
                 "~~order by clientlname desc, invoice_amount desc;"
                 });
               
            }
            else if (Interest == "js1")
            {
                display_code = new List<string>(new string[]{
                 "~var person = {",
                 "~~~firstName:\"John\",",
                 "~~~lastName:\"Doe\",",
                 "~~~age:50,",
                 "~~~eyeColor:\"blue\",",
                 "~};"
                });
               
            }
            else if (Interest == "js2")
            {
                display_code = new List<string>(new string[]{
                 "try",
                "~~{",
                 "~~~~Block of code to try",
                 "~~}",
                "catch (err)",
                "~~{",
                 "~~~~Block of code to handle errors",
                 "~~}"
                });
               
            }
            else
            {
                display_code = new List<string>(new string[] { "No code" });
            }
            int i = 0;
            foreach (string code_line in display_code)
            {
                codeRow = codeData.NewRow();
                codeRow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp");
                codeRow[cod_col.ColumnName] = formatted_code;
                i++;
                codeData.Rows.Add(codeRow);
            }

            DataView codeView = new DataView(codeData);
            return codeView;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataView code_display = SourceCodeDisplay();
            code_Example.DataSource = code_display;
            code_Example.DataBind();
        }
    }
}

        
   
        